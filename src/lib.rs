use twmap::{Color, Error, Point, TwMap, Version, PhysicsLayer};
use twmap::convert::TryTo;

use crate::groups::{GroupData, PyGroups};
use crate::info::PyInfo;
use crate::sequence_wrapping::SequenceInitializer;

use crate::envelopes::{EnvelopeData, PyEnvelopes};
use crate::layers::{PyLayer, PyLayers};
use crate::images::PyImages;
use crate::sounds::PySounds;

use pyo3::prelude::*;
use pyo3::types::PyString;
use pyo3::{create_exception, PyNativeType};

use std::fmt::Display;
use std::ops::DerefMut;
use std::sync::{Arc, Mutex};
use fixed::traits::{Fixed, FromFixed, ToFixed};

mod info;
#[macro_use]
mod sequence_wrapping;
mod envelope_points;
mod envelopes;
mod groups;
mod images;
mod layers;
mod quads;
mod sound_sources;
mod sounds;

#[pyclass(name = "Map")]
#[pyo3(text_signature = "(path)")]
/// Main struct of twmap.
/// The constructor takes a file path.
///
/// Behavior you need to know about:
///     - Use the help on all objects of this module, it will give you thorough documentation!
///         ALSO THE COLLECTION TYPES HAVE DOCUMENTATION! (map.groups, layer.quads, etc.)
///         Only there you will find constructors for the different objects!
///         The help documentation is also the only place where the attributes are listed!
///     - If any attribute returns you a list, you will not be able to edit it in-place.
///         Bind it to a variable, edit the list there, put the list back into that attribute
///
/// Behavior of collection types:
///     - There are many different collection types in by this module, e.g. Groups, Envelopes, etc.
///     - They all have constructor methods you can find in their documentation
///         -> use `help` on an object of that type
///     - They all support the following functions:
///         `len(collection)` returns the length
///         `collection[index]` returns the corresponding element
///         `collection[index] = item` sets the corresponding element, returns the inserted object
///         `del collection[index]` deletes the corresponding element
///         `item in collection` returns if the item is part of the collection
///         `collection.append(item)` appends the item to the collection, returns the inserted object
///         `collection.insert(index, item)` inserts the item at the index (not replacing),
///             returns the newly inserted object
///     - Most have a `new` method (see their documentation), which appends a new item and returns it
///
/// Other behavior:
///     - Constructors will always return the constructed object
///         Also new objects are appended to the end
///     - All python objects can be compared if they are equal
///         This returns if they point to the same layer, not if the underlying layer is equal
///     - Some high-level methods on the struct (like rotate, optimize) will invalidate all
///         objects from the map (like groups, layers, images) which you have bound to variables
///     - You can copy objects from one map into another
///
/// Attributes:
///     info
///     groups
///     envelopes
///     images
///     sounds
#[derive(Clone)]
pub struct PyMap(Arc<Mutex<Map>>);

struct Map {
    map: Arc<Mutex<TwMap>>,
    groups: SequenceInitializer<GroupData, ()>,
    envelopes: SequenceInitializer<EnvelopeData, ()>,
    images: SequenceInitializer<(), ()>,
    sounds: SequenceInitializer<(), ()>,
}

pub fn py_err<T: Into<Error>>(err: T) -> PyErr {
    let err: Error = err.into();
    match err {
        Error::Io(err) => pyo3::exceptions::PyIOError::new_err(err.to_string()),
        _ => MapError::new_err(err.to_string()),
    }
}

pub fn map_err<T: Display>(err: T) -> PyErr {
    MapError::new_err(err.to_string())
}

impl Map {
    fn invalidate_lookup_tables(&mut self) {
        let map = self.map.lock().unwrap();
        if let Some(seq) = self.groups.try_get() {
            seq.lock().unwrap().invalidate(map.groups.len())
        }
        if let Some(seq) = self.envelopes.try_get() {
            seq.lock().unwrap().invalidate(map.envelopes.len())
        }
        if let Some(seq) = self.images.try_get() {
            seq.lock().unwrap().invalidate(map.images.len())
        }
        if let Some(seq) = self.sounds.try_get() {
            seq.lock().unwrap().invalidate(map.sounds.len())
        }
    }

    fn from_twmap(map: TwMap) -> Self {
        let arc_map = Arc::new(Mutex::new(map));
        Map {
            map: arc_map,
            groups: SequenceInitializer::default(),
            envelopes: SequenceInitializer::default(),
            images: SequenceInitializer::default(),
            sounds: SequenceInitializer::default(),
        }
    }

    fn into_py_map(self) -> PyMap {
        PyMap(Arc::new(Mutex::new(self)))
    }
}

#[pymethods]
impl PyMap {
    #[new]
    fn new(path: &str) -> PyResult<Self> {
        TwMap::parse_path(path)
            .map(Map::from_twmap)
            .map(Map::into_py_map)
            .map_err(py_err)
    }

    fn __getattr__(&self, name: &PyString) -> PyResult<PyObject> {
        let mut py_map = self.0.lock().unwrap();
        py_map.getattr(name)
    }

    /// Constructs an empty map object with the specified version.
    /// The parameter 'version' must be one of "DDNet06", "Teeworlds07".
    #[staticmethod]
    #[pyo3(text_signature = "(version, /)")]
    fn empty(version: &str) -> PyResult<Self> {
        let version = match version {
            "Teeworlds07" => Version::Teeworlds07,
            "DDNet06" => Version::DDNet06,
            _ => {
                return Err(pyo3::exceptions::PyValueError::new_err(
                    "Not a valid version, try 'Teeworlds07' or 'DDNet06'",
                ))
            }
        };
        Ok(Map::from_twmap(TwMap::empty(version)).into_py_map())
    }

    /// Save the map to the passed file path
    #[pyo3(text_signature = "($self, path, /)")]
    fn save(&self, path: &str) -> PyResult<()> {
        self.0
            .lock()
            .unwrap()
            .map
            .lock()
            .unwrap()
            .save_file(path)
            .map_err(py_err)
    }

    /// Save the map in the MapDir format the passed file path
    #[pyo3(text_signature = "($self, path, /)")]
    fn save_dir(&self, path: &str) -> PyResult<()> {
        self.0
            .lock()
            .unwrap()
            .map
            .lock()
            .unwrap()
            .save_dir(path)
            .map_err(py_err)
    }

    /// Embed the external images using the mapres in the provided directory
    #[pyo3(text_signature = "($self, path, /)")]
    fn embed_images(&self, mapres_directory: &str) -> PyResult<()> {
        self.0
            .lock()
            .unwrap()
            .map
            .lock()
            .unwrap()
            .embed_images(mapres_directory)
            .map_err(map_err)
    }

    /// Rotates the map clockwise
    #[pyo3(text_signature = "($self, /)")]
    fn rotate(&mut self) -> PyResult<()> {
        let mut py_map = self.0.lock().unwrap();
        {
            let mut map = py_map.map.lock().unwrap();
            map.load().map_err(map_err)?;
            match map.clone().rotate_right() {
                None => {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        "Overflow occurred while rotating",
                    ))
                }
                Some(rotated) => *map.deref_mut() = rotated,
            }
        }
        py_map.invalidate_lookup_tables();
        Ok(())
    }

    /// Mirrors the map along the horizontal axis (left <-> right)
    #[pyo3(text_signature = "($self, /)")]
    fn mirror(&mut self) -> PyResult<()> {
        let mut py_map = self.0.lock().unwrap();
        {
            let mut map = py_map.map.lock().unwrap();
            map.load().map_err(map_err)?;
            match map.clone().mirror() {
                None => {
                    return Err(pyo3::exceptions::PyValueError::new_err(
                        "Overflow occurred while mirroring",
                    ))
                }
                Some(mirrored) => *map.deref_mut() = mirrored,
            }
        }
        py_map.invalidate_lookup_tables();
        Ok(())
    }

    /// Returns 'DDNet06' or 'Teeworlds07', depending on the parsed map
    #[pyo3(text_signature = "($self, /)")]
    fn version(&self) -> &'static str {
        match self.0.lock().unwrap().map.lock().unwrap().version {
            Version::DDNet06 => "DDNet06",
            Version::Teeworlds07 => "Teeworlds07",
        }
    }

    /// Returns the game layer (this shouldn't fail since every map has one)
    #[pyo3(text_signature = "($self, /)")]
    fn game_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::GameLayer>(self, py)
    }

    /// Returns the front layer or None
    #[pyo3(text_signature = "($self, /)")]
    fn front_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::FrontLayer>(self, py)
    }

    /// Returns the tele layer or None
    #[pyo3(text_signature = "($self, /)")]
    fn tele_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::TeleLayer>(self, py)
    }

    /// Returns the speedup layer or None
    #[pyo3(text_signature = "($self, /)")]
    fn speedup_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::SpeedupLayer>(self, py)
    }

    /// Returns the switch layer or None
    #[pyo3(text_signature = "($self, /)")]
    fn switch_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::SwitchLayer>(self, py)
    }

    /// Returns the tune layer or None
    #[pyo3(text_signature = "($self, /)")]
    fn tune_layer(&self, py: Python) -> PyResult<Option<PyLayer>> {
        get_py_physics_layer::<twmap::TuneLayer>(self, py)
    }
}

impl Map {
    fn getattr(&mut self, name: &PyString) -> PyResult<PyObject> {
        let py = name.py();
        let name = name.to_str()?;
        let map = self.map.lock().unwrap();
        Ok(match name {
            "info" => PyInfo {
                map: self.map.clone(),
            }
            .into_py(py),
            "groups" => {
                PyGroups(self.groups.get(map.groups.len(), self.map.clone(), ())).into_py(py)
            }
            "envelopes" => PyEnvelopes(self.envelopes.get(
                map.envelopes.len(),
                self.map.clone(),
                (),
            ))
            .into_py(py),
            "images" => {
                PyImages(self.images.get(map.images.len(), self.map.clone(), ())).into_py(py)
            }
            "sounds" => {
                PySounds(self.sounds.get(map.sounds.len(), self.map.clone(), ())).into_py(py)
            }
            _ => {
                return Err(pyo3::exceptions::PyAttributeError::new_err(format!(
                    "The twmap.Map object has no attribute '{}'",
                    name
                )))
            }
        })
    }
}

fn get_py_physics_layer<T: PhysicsLayer>(
    py_map: &PyMap,
    py: Python,
) -> PyResult<Option<PyLayer>> {
    let (group_index, layer_index) = {
        let py_map = py_map.0.lock().unwrap();
        let map = py_map.map.lock().unwrap();
        match map
            .groups
            .iter()
            .enumerate()
            .rev()
            .flat_map(|(group_index, group)| {
                group
                    .layers
                    .iter()
                    .enumerate()
                    .rev()
                    .filter_map(move |(layer_index, layer)| {
                        if T::get(layer).is_some() {
                            Some((group_index, layer_index))
                        } else {
                            None
                        }
                    })
            })
            .next()
        {
            None => return Ok(None),
            Some((group_index, layer_index)) => (group_index, layer_index),
        }
    };
    Ok(Some(
        py_map
            .__getattr__(PyString::new(py, "groups"))
            .unwrap()
            .extract::<PyGroups>(py)
            .unwrap()
            .__getitem__(group_index.try_to())
            .unwrap()
            .__getattr__(PyString::new(py, "layers"))
            .unwrap()
            .extract::<PyLayers>(py)
            .unwrap()
            .__getitem__(layer_index.try_to())
            .unwrap(),
    ))
}

fn fixed_py<T: Fixed>(n: T) -> f64{
    f64::from_fixed(n)
}

fn py_fixed<T: Fixed + Display>(f: f64) -> PyResult<T> {
    f.checked_to_fixed().ok_or_else(|| pyo3::exceptions::PyValueError::new_err(format!("Value {} is too large, it must be between {} and {}", f, T::MIN, T::MAX)))
}

pub fn point_to_tup<T: Fixed>(p: Point<T>) -> (f64, f64) where f64: From<T> {
    (fixed_py(p.x), fixed_py(p.y))
}

pub fn tup_to_point<T: Fixed>(t: (f64, f64)) -> PyResult<Point<T>> {
    Ok(Point { x: py_fixed(t.0)?, y: py_fixed(t.1)? })
}

pub fn depy_pos<T: Fixed>(value: PyObject, py: Python) -> PyResult<Point<T>> {
    let tup = value.extract(py)?;
    tup_to_point(tup)
}

pub fn py_col(color: Color) -> (u8, u8, u8, u8) {
    (color.r, color.g, color.b, color.a)
}

pub fn col_from_tuple(tup: (u8, u8, u8, u8)) -> PyResult<Color> {
    Ok(Color {
        r: tup.0,
        g: tup.1,
        b: tup.2,
        a: tup.3,
    })
}

pub fn depy_col(value: PyObject, py: Python) -> PyResult<Color> {
    let tup = value.extract(py)?;
    col_from_tuple(tup)
}

/// The aspect ratio is width / height.
/// Returns the dimension in tiles that Teeworlds/DDNet would render in that aspect ratio.
/// Default zoom level is assumed.
#[pyfunction]
#[pyo3(text_signature = "(aspect_ratio, /)")]
fn camera_dimensions(aspect_ratio: f32) -> (f32, f32) {
    let dimensions = twmap::edit::camera_dimensions(aspect_ratio);
    (dimensions.x, dimensions.y)
}

/// The maximum amount of tiles horizontally and vertically that the camera can cover.
/// Note that it can never be the maximum in both directions.
/// Default zoom level is assumed.
#[pyfunction]
#[pyo3(text_signature = "(/)")]
fn max_camera_dimensions() -> (f64, f64) {
    let max = twmap::edit::MAX_CAMERA_DIMENSIONS;
    (fixed_py(max.x), fixed_py(max.y))
}

create_exception!(module, MapError, pyo3::exceptions::PyException);

#[pymodule]
#[pyo3(name = "twmap")]
fn twmap_module(py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyMap>()?;
    m.add("MapError", py.get_type::<MapError>())?;
    m.add_function(wrap_pyfunction!(camera_dimensions, m)?)?;
    m.add_function(wrap_pyfunction!(max_camera_dimensions, m)?)?;

    Ok(())
}
