TwMap Python Module
===

Safely parse, edit and save [Teeworlds](https://www.teeworlds.com/) and [DDNet](https://ddnet.tw/) maps - in Python!

See the Rust library [twmap](https://gitlab.com/Patiga/twmap) for details on the wrapped library.

Installation
---

Simply do `pip install twmap`!

Usage
---

The `help` function is your friend!
Once you imported `twmap`, do `help(twmap.Map)` to access the documentation of the `Map` struct.
It also contains important general knowledge about the module.

To load your first map, do `map = twmap.Map(path-to-map)`.
To see the attributes and documentation about an object... `help` helps you!

For example, have a look into `help(map.groups)` and `help(map.groups[0])`.
The first one will give you methods on the group collection and the second one attributes and methods on group objects.
If you are confused about something in the module, the general information in `help(twmap.Map)` might help you.

Manual Building (on Linux)
---

You need [Rust](https://www.rust-lang.org/tools/install) installed on your system.
To compile twmap in release mode, execute the following command in the source root:
```
cargo build --release
```
Locate the file `target/release/libtwmap.so`.
This file is only importable under `twmap.so`.
Symlink or copy it to your desired location.

Publishing Steps
---

These steps are the ones I use to upload new versions of twmap-py to [pypi](https://pypi.org/).

**Linux**:

1. `docker pull ghcr.io/pyo3/maturin`
2. `docker run --rm -itv $(pwd):/io --entrypoint /bin/bash ghcr.io/pyo3/maturin`
3. `maturin list-python` to list the available interpreter versions
5. Enter pypi credentials into the environment variables `MATURIN_USERNAME` and `MATURIN_PASSWORD`
4. `maturin publish --interpreter <interpreter-version>` for each interpreter version

**Cross-compiling to Windows**:

1. `Cargo.toml`: switch the `pyo3` dependency line
2. Download the latest Python 3.7 "Windows x86-64 embeddable zip file" from https://www.python.org/downloads/windows/. Last used: https://www.python.org/ftp/python/3.7.9/python-3.7.9-embed-amd64.zip
3. Unzip in project root
4. `PYO3_CROSS_LIB_DIR="./python-3.7.9-embed-amd64" maturin publish --target x86_64-pc-windows-gnu` (replace the dir path if another version is used)
5. Enter pypi credentials interactively
